﻿using System.Collections.Generic;

namespace DataModels
{
    public class SpecificationAttribute
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int DisplayOrder { get; set; }

        public int xyTypeId { get; set; }

        public bool xyIsFilter { get; set; }

        public string xyDisplayName { get; set; }

        public virtual ICollection<SpecificationAttributeOption> SpecificationAttributeOptions { get; protected set; }
    }
}
