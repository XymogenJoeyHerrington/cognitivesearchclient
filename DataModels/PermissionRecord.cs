﻿using System.Collections.Generic;

namespace DataModels
{
    public class PermissionRecord
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string SystemName { get; set; }

        public string Category { get; set; }

        public virtual ICollection<CustomerRole> CustomerRoles { get; protected set; }
        public virtual ICollection<PermissionRecordCustomerRoleMapping> PermissionRecordCustomerRoleMappings { get; protected set; }
    }
}