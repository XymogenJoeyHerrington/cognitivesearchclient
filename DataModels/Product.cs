﻿using System;
using System.Collections.Generic;
using DataModels.Enums;

namespace DataModels
{
    public class Product
    {
        public int Id { get; set; }

        public ProductType ProductType { get; set; }

        public int ParentGroupedProductId { get; set; }

        public bool VisibleIndividually { get; set; }

        public string Name { get; set; }

        public string ShortDescription { get; set; }

        public string FullDescription { get; set; }

        public string AdminComment { get; set; }

        public int ProductTemplateId { get; set; }

        public int VendorId { get; set; }

        public bool ShowOnHomePage { get; set; }

        public string MetaKeywords { get; set; }

        public string MetaDescription { get; set; }

        public string MetaTitle { get; set; }

        public bool AllowCustomerReviews { get; set; }

        public int ApprovedRatingSum { get; set; }

        public int NotApprovedRatingSum { get; set; }

        public int ApprovedTotalReviews { get; set; }

        public int NotApprovedTotalReviews { get; set; }

        public bool SubjectToAcl { get; set; }

        public bool LimitedToStores { get; set; }

        public string Sku { get; set; }

        public string ManufacturerPartNumber { get; set; }

        public string Gtin { get; set; }

        public bool IsGiftCard { get; set; }

        public GiftCardType GiftCardType { get; set; }

        public decimal? OverriddenGiftCardAmount { get; set; }

        public bool RequireOtherProducts { get; set; }

        public string RequiredProductIds { get; set; }

        public bool AutomaticallyAddRequiredProducts { get; set; }

        public bool IsDownload { get; set; }

        public int DownloadId { get; set; }

        public bool UnlimitedDownloads { get; set; }

        public int MaxNumberOfDownloads { get; set; }

        public int? DownloadExpirationDays { get; set; }

        public DownloadActivationType DownloadActivationType { get; set; }

        public bool HasSampleDownload { get; set; }

        public int SampleDownloadId { get; set; }

        public bool HasUserAgreement { get; set; }

        public string UserAgreementText { get; set; }

        public bool IsRecurring { get; set; }

        public int RecurringCycleLength { get; set; }

        public RecurringProductCyclePeriod RecurringCyclePeriod { get; set; }

        public int RecurringTotalCycles { get; set; }

        public bool IsRental { get; set; }

        public int RentalPriceLength { get; set; }

        public RentalPricePeriod RentalPricePeriod { get; set; }

        public bool IsShipEnabled { get; set; }

        public bool IsFreeShipping { get; set; }

        public bool ShipSeparately { get; set; }

        public decimal AdditionalShippingCharge { get; set; }

        public int DeliveryDateId { get; set; }

        public bool IsTaxExempt { get; set; }

        public int TaxCategoryId { get; set; }

        public bool IsTelecommunicationsOrBroadcastingOrElectronicServices { get; set; }

        public ManageInventoryMethod ManageInventoryMethod { get; set; }

        public int ProductAvailabilityRangeId { get; set; }

        public bool UseMultipleWarehouses { get; set; }

        public int WarehouseId { get; set; }

        public int StockQuantity { get; set; }

        public bool DisplayStockAvailability { get; set; }

        public bool DisplayStockQuantity { get; set; }

        public int MinStockQuantity { get; set; }

        public LowStockActivity LowStockActivity { get; set; }

        public int NotifyAdminForQuantityBelow { get; set; }

        public BackorderMode BackorderMode { get; set; }

        public bool AllowBackInStockSubscriptions { get; set; }

        public int OrderMinimumQuantity { get; set; }

        public int OrderMaximumQuantity { get; set; }

        public string AllowedQuantities { get; set; }

        public bool AllowAddingOnlyExistingAttributeCombinations { get; set; }

        public bool NotReturnable { get; set; }

        public bool DisableBuyButton { get; set; }

        public bool DisableWishlistButton { get; set; }

        public bool AvailableForPreOrder { get; set; }

        public DateTime? PreOrderAvailabilityStartDateTimeUtc { get; set; }

        public bool CallForPrice { get; set; }

        public decimal Price { get; set; }

        public decimal OldPrice { get; set; }

        public decimal ProductCost { get; set; }

        public bool CustomerEntersPrice { get; set; }

        public decimal MinimumCustomerEnteredPrice { get; set; }

        public decimal MaximumCustomerEnteredPrice { get; set; }

        public bool BasepriceEnabled { get; set; }

        public decimal BasepriceAmount { get; set; }

        public int BasepriceUnitId { get; set; }

        public decimal BasepriceBaseAmount { get; set; }

        public int BasepriceBaseUnitId { get; set; }

        public bool MarkAsNew { get; set; }

        public DateTime? MarkAsNewStartDateTimeUtc { get; set; }

        public DateTime? MarkAsNewEndDateTimeUtc { get; set; }

        public bool HasDiscountsApplied { get; set; }

        public decimal Weight { get; set; }

        public decimal Length { get; set; }

        public decimal Width { get; set; }

        public decimal Height { get; set; }

        public DateTime? AvailableStartDateTimeUtc { get; set; }

        public DateTime? AvailableEndDateTimeUtc { get; set; }

        public int DisplayOrder { get; set; }

        public bool Published { get; set; }

        public bool Deleted { get; set; }

        public string PLMedPaxBP { get; set; }

        public DateTime CreatedOnUtc { get; set; }

        public DateTime UpdatedOnUtc { get; set; }

        public bool xyIsPrivateLabel { get; set; }

        public bool xyIsFeatured { get; set; }

        public bool xyIsBestSeller { get; set; }

        public bool xyNonDiscountable { get; set; }

        public string xySimilarItems { get; set; }

        public int? xyNumberSold { get; set; }

        public virtual ICollection<Category> Categories { get; protected set; }
        public virtual ICollection<ProductCategory> ProductCategories { get; protected set; }

        public virtual ICollection<Picture> Pictures { get; protected set; }
        public virtual ICollection<ProductPicture> ProductPictures { get; protected set; }

        public virtual ICollection<ProductSpecificationAttribute> ProductSpecificationAttributes { get; protected set; }

        public virtual ICollection<ProductAttribute> ProductAttributes { get; protected set; }
        public virtual ICollection<ProductAttributeMapping> ProductAttributeMappings { get; protected set; }

        public virtual ICollection<TierPrice> TierPrices { get; protected set; }
    }

}
