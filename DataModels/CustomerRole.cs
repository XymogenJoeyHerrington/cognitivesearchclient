﻿using System.Collections.Generic;

namespace DataModels
{
    public class CustomerRole
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public bool FreeShipping { get; set; }

        public bool TaxExempt { get; set; }

        public bool Active { get; set; }

        public bool IsSystemRole { get; set; }

        public string SystemName { get; set; }

        public bool EnablePasswordLifetime { get; set; }

        public bool OverrideTaxDisplayType { get; set; }

        public int DefaultTaxDisplayTypeId { get; set; }

        public int PurchasedWithProductId { get; set; }

        public virtual ICollection<TierPrice> TierPrices { get; protected set; }

        public virtual ICollection<PermissionRecord> PermissionRecords { get; protected set; }
        public virtual ICollection<PermissionRecordCustomerRoleMapping> PermissionRecordCustomerRoleMappings { get; protected set; }
    }
}