﻿using System.Collections.Generic;

namespace DataModels
{
    public class ProductAttribute
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int xyTypeId { get; set; }

        public virtual ICollection<Product> Products { get; protected set; }
        public virtual ICollection<ProductAttributeMapping> ProductAttributeMappings { get; protected set; }
    }
}