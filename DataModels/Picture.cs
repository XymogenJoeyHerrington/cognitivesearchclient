﻿using System.Collections.Generic;

namespace DataModels
{
    public class Picture
    {
        public int Id { get; set; }

        public string MimeType { get; set; }

        public string SeoFilename { get; set; }

        public string AltAttribute { get; set; }

        public string TitleAttribute { get; set; }

        public bool IsNew { get; set; }

        public virtual PictureBinary PictureBinary { get; set; }

        public virtual ICollection<Product> Products { get; protected set; }
        public virtual ICollection<ProductPicture> ProductPictures { get; protected set; }
    }
}
