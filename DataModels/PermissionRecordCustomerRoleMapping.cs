﻿namespace DataModels
{
    public class PermissionRecordCustomerRoleMapping
    {
        public int PermissionRecordId { get; set; }

        public int CustomerRoleId { get; set; }

        public virtual PermissionRecord PermissionRecord { get; set; }

        public virtual CustomerRole CustomerRole { get; set; }
    }
}