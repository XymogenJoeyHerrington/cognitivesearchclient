﻿namespace DataModels.Enums
{
    public enum LowStockActivity
    {
        Nothing = 0,
        DisableBuyButton = 1,
        Unpublish = 2
    }
}
