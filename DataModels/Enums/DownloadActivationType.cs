﻿namespace DataModels.Enums
{
    public enum DownloadActivationType
    {
        WhenOrderIsPaid = 0,
        Manually = 10
    }
}
