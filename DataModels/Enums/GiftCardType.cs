﻿namespace DataModels.Enums
{
    public enum GiftCardType
    {
        Virtual = 0,
        Physical = 1
    }
}
