﻿using DataModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataService.ModelConfiguration
{
    public class CustomerRoleConfiguration : IEntityTypeConfiguration<CustomerRole>
    {
        public void Configure(EntityTypeBuilder<CustomerRole> builder)
        {
            builder.ToTable(nameof(CustomerRole));

            builder
                .HasMany(t => t.PermissionRecords)
                .WithMany(t => t.CustomerRoles)
                .UsingEntity<PermissionRecordCustomerRoleMapping>(
                    configureRight: v => v
                        .HasOne(prop => prop.PermissionRecord)
                        .WithMany(prop => prop.PermissionRecordCustomerRoleMappings)
                        .HasForeignKey(prop => prop.PermissionRecordId),
                    configureLeft: v => v
                        .HasOne(prop => prop.CustomerRole)
                        .WithMany(prop => prop.PermissionRecordCustomerRoleMappings)
                        .HasForeignKey(prop => prop.CustomerRoleId),
                    configureJoinEntityType: v =>
                    {
                        v.ToTable("PermissionRecord_Role_Mapping");
                        v.Property(t => t.PermissionRecordId).HasColumnName("PermissionRecord_Id");
                        v.Property(t => t.CustomerRoleId).HasColumnName("CustomerRole_Id");
                    });

        }
    }
}
