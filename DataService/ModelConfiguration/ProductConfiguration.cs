﻿using DataModels;
using DataModels.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataService.ModelConfiguration
{
    public class ProductConfiguration : IEntityTypeConfiguration<Product>
    {
        public void Configure(EntityTypeBuilder<Product> builder)
        {
            builder.ToTable(nameof(Product));

            builder.Property(t => t.ProductType)
                .HasColumnName("ProductTypeId")
                .HasConversion(
                    v => (int)v,
                    v => (ProductType)v);

            builder.Property(t => t.BackorderMode)
                .HasColumnName("BackorderModeId")
                .HasConversion(
                    v => (int)v,
                    v => (BackorderMode)v);

            builder.Property(t => t.DownloadActivationType)
                .HasColumnName("DownloadActivationTypeId")
                .HasConversion(
                    v => (int)v,
                    v => (DownloadActivationType)v);

            builder.Property(t => t.GiftCardType)
                .HasColumnName("GiftCardTypeId")
                .HasConversion(
                    v => (int)v,
                    v => (GiftCardType)v);

            builder.Property(t => t.LowStockActivity)
                .HasColumnName("LowStockActivityId")
                .HasConversion(
                    v => (int)v,
                    v => (LowStockActivity)v);


            builder.Property(t => t.ManageInventoryMethod)
                .HasColumnName("ManageInventoryMethodId")
                .HasConversion(
                    v => (int)v,
                    v => (ManageInventoryMethod)v);

            builder.Property(t => t.RecurringCyclePeriod)
                .HasColumnName("RecurringCyclePeriodId")
                .HasConversion(
                    v => (int)v,
                    v => (RecurringProductCyclePeriod)v);


            builder.Property(t => t.RentalPricePeriod)
                .HasColumnName("RentalPricePeriodId")
                .HasConversion(
                    v => (int)v,
                    v => (RentalPricePeriod)v);

            builder
                .HasMany(t => t.Categories)
                .WithMany(t => t.Products)
                .UsingEntity<ProductCategory>(
                    configureRight: v => v
                        .HasOne(prop => prop.Category)
                        .WithMany(prop => prop.ProductCategories)
                        .HasForeignKey(prop => prop.CategoryId),
                    configureLeft: v => v
                        .HasOne(prop => prop.Product)
                        .WithMany(prop => prop.ProductCategories)
                        .HasForeignKey(prop => prop.ProductId),
                    configureJoinEntityType: v =>
                    {
                        v.ToTable("Product_Category_Mapping");
                    });

            builder
                .HasMany(t => t.Pictures)
                .WithMany(t => t.Products)
                .UsingEntity<ProductPicture>(
                    configureRight: v => v
                        .HasOne(prop => prop.Picture)
                        .WithMany(prop => prop.ProductPictures)
                        .HasForeignKey(prop => prop.PictureId),
                    configureLeft: v => v
                        .HasOne(prop => prop.Product)
                        .WithMany(prop => prop.ProductPictures)
                        .HasForeignKey(prop => prop.ProductId),
                    configureJoinEntityType: v =>
                    {
                        v.ToTable("Product_Picture_Mapping");
                    });

            builder
                .HasMany(t => t.ProductAttributes)
                .WithMany(t => t.Products)
                .UsingEntity<ProductAttributeMapping>(
                    configureRight: v => v
                        .HasOne(prop => prop.ProductAttribute)
                        .WithMany(prop => prop.ProductAttributeMappings)
                        .HasForeignKey(prop => prop.ProductAttributeId),
                    configureLeft: v => v
                        .HasOne(prop => prop.Product)
                        .WithMany(prop => prop.ProductAttributeMappings)
                        .HasForeignKey(prop => prop.ProductId),
                    configureJoinEntityType: v =>
                    {
                        v.ToTable("Product_ProductAttribute_Mapping");
                    });

            builder
                .HasMany(t => t.ProductSpecificationAttributes)
                .WithOne(t => t.Product)
                .HasForeignKey(t => t.ProductId);

            builder
                .HasMany(t => t.TierPrices)
                .WithOne(t => t.Product)
                .HasForeignKey(t => t.ProductId);



        }
    }
}
