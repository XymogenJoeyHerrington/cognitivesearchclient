﻿using DataModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataService.ModelConfiguration
{
    public class SpecificationAttributeOptionConfiguration : IEntityTypeConfiguration<SpecificationAttributeOption>
    {
        public void Configure(EntityTypeBuilder<SpecificationAttributeOption> builder)
        {
            builder.ToTable(nameof(SpecificationAttributeOption));

            builder
                .HasOne(t => t.SpecificationAttribute)
                .WithMany(t => t.SpecificationAttributeOptions)
                .HasForeignKey(t => t.SpecificationAttributeId)
                .IsRequired();
        }
    }
}
