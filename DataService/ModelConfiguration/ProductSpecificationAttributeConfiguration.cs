﻿using DataModels;
using DataModels.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataService.ModelConfiguration
{
    public class ProductSpecificationAttributeConfiguration : IEntityTypeConfiguration<ProductSpecificationAttribute>
    {
        public void Configure(EntityTypeBuilder<ProductSpecificationAttribute> builder)
        {
            builder.ToTable("Product_SpecificationAttribute_Mapping");

            builder
                .Property(t => t.AttributeType)
                .HasColumnName("AttributeTypeId")
                .HasConversion(
                    v => (int)v,
                    v => (SpecificationAttributeType)v);

            builder
                .HasOne(t => t.SpecificationAttributeOption)
                .WithMany()
                .HasForeignKey(prop => prop.SpecificationAttributeOptionId);
        }
    }
}
