﻿using DataModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataService.ModelConfiguration
{
    public class TierPriceConfiguration : IEntityTypeConfiguration<TierPrice>
    {
        public void Configure(EntityTypeBuilder<TierPrice> builder)
        {
            builder.ToTable(nameof(TierPrice));

            builder
                .HasOne(t => t.CustomerRole)
                .WithMany(t => t.TierPrices)
                .HasForeignKey(price => price.CustomerRoleId)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
