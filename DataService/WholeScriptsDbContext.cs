﻿using DataModels;
using Microsoft.EntityFrameworkCore;

namespace DataService
{
    public class WholeScriptsDbContext : DbContext
    {
        public DbSet<Product> Products { get; set; }

        #region setup

        public WholeScriptsDbContext(DbContextOptions<WholeScriptsDbContext> options)
            : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(WholeScriptsDbContext).Assembly);
        }
        #endregion setup
    }
}
