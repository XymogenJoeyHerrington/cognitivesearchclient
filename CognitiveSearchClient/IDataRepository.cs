﻿using System.Linq;
using DataModels;

namespace CognitiveSearchClient
{
    public interface IDataRepository
    {
        IQueryable<Product> Products();
    }
}
