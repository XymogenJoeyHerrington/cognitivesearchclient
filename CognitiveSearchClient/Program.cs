﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataService;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace CognitiveSearchClient
{
    class Program
    {
        static async Task Main(string[] args)
        {
            IServiceCollection serviceCollection = new ServiceCollection()
                .AddDbContext<WholeScriptsDbContext>(options => options.UseSqlServer("Data Source=52.168.73.91;Initial Catalog=WholeScriptsNop410_LOAD;Integrated Security=False;Persist Security Info=False;User ID=teamxymo;Password=1Mp@55ibL3"))
                .AddTransient<IDataRepository, DataRepository>();

            var services = serviceCollection.BuildServiceProvider();

            await GetProducts(services.GetService<IDataRepository>());
        }

        public static async Task GetProducts(IDataRepository repository)
        {
            var products = await repository.Products()
                .FirstOrDefaultAsync(p => p.Id == 14155);

            var searchTerms = (products.MetaKeywords
                ?.Split(',') ?? Enumerable.Empty<string>())
                .Append(products.Name)
                .ToList();

            var similarTerms = (products.xySimilarItems
                ?.Split(',') ?? Enumerable.Empty<string>())
                .ToList();

            var Prices = products.TierPrices
                .Select(p => new
                {
                    RoleId = p.CustomerRoleId,
                    Role = p.CustomerRole.Name,
                    Price = p.Price
                })
                .ToList();

            var categories = products.Categories
                .Select(c => new
                {
                    c.Id,
                    c.Name
                })
                .ToList();

            var attributes = products.ProductAttributes
                .Select(p => new { p.Id, p.Name })
                .ToList();

            var specificationAttributes = products.ProductSpecificationAttributes
                .Where(p=>p.AttributeType == DataModels.Enums.SpecificationAttributeType.Option)
                .Select(p => new { p.Id, p.SpecificationAttributeOption.Name, Value = p.CustomValue })
                .ToList();
        }
    }
}
/*
    "Name": "6 Day Detox Kit",
    "Id": 14155,
    "Sku": "000000000500070044",
    "Gtin": null,
    "SearchString": "6DayDetoxKit,XYMOGEN",
    "Quantity": 1,
    "Msrp": 145.4000,
    "Price": 86.2000,
    "TrackInventory": false,
    "StockQuantity": 1000,
    "StockAvailability": "In stock",
    "DisableBuyButton": false,
    "IsPrivateLabelProduct": false,
    "IsPrivateLabelOnBackOrder": false,
    "ProductUrl": "/6-Day-Detox-Kit",
    "ProductImageUrl": "/images/thumbs/0005281_6-day-detox-kit_200.png",
    "ShortDescription": null,
    "FullDescription": "<p><span class=\"s1\"><strong>6 Day Detox Kit</strong></span> is designed to renew and enhance the body’s cleansing and detoxification capabilities. It combines four specially selected XYMOGEN formulations: ColonX™, Drainage™, ProbioMax<sup>®</sup> Daily DF, and OptiCleanse<sup>®</sup> GHI. When taken together, as recommended within the 6-Day Detox Guide, these complementary formulas work in concert to encourage the various detox-linked systems and organs of the body to process and eliminate waste and toxins.*</p>\r\n<p> </p>\r\n<p><strong>6 Day Detox Kit</strong> includes 1 bottle of OptiCleanse GHI Vanilla Delight, 1 bottle of ColonX 60c, 1 carton of ProbioMax Daily DF 30c, 1 bottle of Drainage 1 fl oz, and 1 20 oz Shaker Bottle.</p>",
    "IsNewProduct": false,
    "IsFeatured": false,
    "IsBestSeller": false,
    "SimilarItems": null,
    "NumberSold": null,
    "QuantityPerCase": 0,
    "CategoryIds": [
      1,
      16
    ],
    "CategoryNames": [
      "All",
      "Detoxification"
    ],
    "SpecificationAttributeOptionIds": [
      20101
    ],
    "SerializedAttributesForm": "product_attribute_14522=7770",
    "FormattedAttributes": "<select id=\"productAttribute_14522\" name=\"productAttribute_14522\"><option value=\"7770\">One-Time</option><option value=\"1649289\">Every 30 Days</option><option value=\"1649290\">Every 60 Days</option><option value=\"1649288\">Every 90 Days</option></select>\r\n"



FilterClear Filter(s)

Sort By
    Aphabetical
    Best Sellers
    Price: low to high
    Price: high to low

By Status
    New
    Favorites
    Featured
    Recommended
    By A-Z

By Health Category
    All
    Adrenal Support
    Antioxidant Activity
    Blood Sugar Support
    Body Composition
    Bone Health Support
    Cardiovascular Support
    Cell-Life Regulation
    Cytokine Balance Support
    Detoxification
    Essential Fatty Acids
    Female Health
    Gastrointestinal Support
    Immune System Support
    Joint & Muscle Support
    Liver Support
    Male Health
    Multivitamins & Minerals
    Neurologic & Cognitive
    Probiotics
    Relaxation & Sleep
    Sports Nutrition
    Thyroid Support
    By Delivery Form
    Bars
    Capsules
    Cookies
    Fluid Ounces
    Liquids / Creams
    Lozenges
    Oil
    Packets
    Powders
    Sachets
    Softgels
    Tablets
    Wafers

By Brand
    American BioSciences
    AOR
    AVEMAR™
    Avior
    Bio-Tech Pharmacal
    BodyBio
    Cognitive Clarity
    Dr. Phillips
    Drs Gr8 Bar
    Encore Life
    Endurance Products Company
    Even Health
    Everidis Health Sciences
    Golden Flower Chinese Herbs
    Herbalist & Alchemist
    Linpharma
    Nature's Sources
    Neuropathy Factor
    NuGo Nutrition
    NutriDyn
    Nutritional Therapeutics
    Quicksilver
    Transformation Enzyme
    TruGen3
    Vitanica
    XYMOGEN®
    Zhang Health

 */