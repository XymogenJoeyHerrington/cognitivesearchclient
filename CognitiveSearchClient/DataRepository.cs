﻿using System.Linq;
using DataModels;
using DataService;
using Microsoft.EntityFrameworkCore;

namespace CognitiveSearchClient
{
    public class DataRepository : IDataRepository
    {
        private readonly WholeScriptsDbContext _dbContext;

        public DataRepository(WholeScriptsDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IQueryable<Product> Products() => _dbContext.Products
            .Include(p => p.ProductSpecificationAttributes)
                .ThenInclude(p => p.SpecificationAttributeOption)
                .ThenInclude(p => p.SpecificationAttribute)
            .Include(p => p.TierPrices)
                .ThenInclude(p => p.CustomerRole)
            .Include(p => p.ProductAttributes)
            .Include(p => p.Categories);
    }
}
